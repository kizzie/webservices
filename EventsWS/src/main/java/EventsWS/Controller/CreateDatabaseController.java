package EventsWS.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import EventsWS.Model.EventsDAO;

/*
 * Maps to the root, calls the createDatabase() method
 * on the DAO, only needed when using the SQL database
 * for the first time.
 */
@RestController
public class CreateDatabaseController {


	@Autowired
	EventsDAO dao;

	@RequestMapping("/")
	public void createDB() {
		dao.createDatabase();
	}
}
