import java.util.ArrayList;

import model.beans.Event;
import model.beans.User;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Main {

	public static void main(String[] args) {
		//create the context - this creates the beans for us
		AnnotationConfigApplicationContext context  = new AnnotationConfigApplicationContext(Config.class);
		
		//get the beans
		
		Event eventOne = (Event) context.getBean("getEventOne");
		Event eventTwo = (Event) context.getBean("getEventTwo");
		
		//get users as a list
		ArrayList<User> userList = new ArrayList<User>(context.getBeansOfType(User.class).values());
		
		//print them out (you may want a tostring method in your beans!)
		System.out.println(eventOne.getName());
		System.out.println(eventTwo.getName());
		
		for(User u : userList) {
			System.out.println(u.getName());
		}

	}
}
