import java.util.Date;

import model.beans.Event;
import model.beans.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Config {

	
	@Bean 
	public Event getEventOne(){
		Event e = new Event(0, "Birthday", "Birthday Party", new Date(), 15);
		e.addUser(getUserTwo());
		e.addUser(getUserThree());
		return e;
	}
	
	@Bean 
	public Event getEventTwo(){
		Event e = new Event(1, "Java Party", "For all your programming needs", new Date(), 8);
		e.addUser(getUserOne());
		return e;
	}
	
	@Bean
	public User getUserOne(){
		return new User(0, "Alice", "alice@email.com");
	}
	
	@Bean
	public User getUserTwo(){
		return new User(0, "Bob", "bob@email.com");
	}
	
	@Bean
	public User getUserThree(){
		return new User(0, "Eve", "eve@email.com");
	}
	
	
}
